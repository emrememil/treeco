import 'package:jiffy/jiffy.dart';
import 'package:treeco/core/constants/enums/locale_keys_enum.dart';
import 'package:treeco/core/dal/network/UserOperations/user_operation_service.dart';
import 'package:treeco/core/init/cache/locale_manager.dart';
import 'package:treeco/core/models/user_information.dart';
import 'package:treeco/core/constants/app/GlobalCache.dart';
import 'package:treeco/view/home/view_model/home_view_model.dart';


class LoginDays{

  int thisDay;
  int lastDay;
  static int counterOfConsecutiveDays;

  UserOperationService _userOperationService = UserOperationService(null);


  void sharedPreferencesOperations(){
    thisDay = Jiffy().dayOfYear;
    lastDay = LocaleManager.instance.getIntValue(PreferencesKeys.LAST_LOGIN_DAY);
    counterOfConsecutiveDays = LocaleManager.instance.getIntValue(PreferencesKeys.COUNTER_OF_CONSECUTIVE_DAYS);

    if(lastDay == thisDay-1){
      counterOfConsecutiveDays = counterOfConsecutiveDays+1;
      LocaleManager.instance.setIntValue(PreferencesKeys.LAST_LOGIN_DAY, thisDay);
      LocaleManager.instance.setIntValue(PreferencesKeys.COUNTER_OF_CONSECUTIVE_DAYS, counterOfConsecutiveDays);
    }
    else if(lastDay == thisDay){
      //Bir gün içerisinde birden fazla giriş
    }
    else{
      LocaleManager.instance.setIntValue(PreferencesKeys.LAST_LOGIN_DAY, thisDay);
      LocaleManager.instance.setIntValue(PreferencesKeys.COUNTER_OF_CONSECUTIVE_DAYS, 1);
    }
  }

  Future<UserInformationModel> updateCoin(HomeViewModel viewModel, int newCoin) async{
    var userInformation = GlobalCache.userInformation;
    if(userInformation != null){
      userInformation.coin += newCoin;
      var response = await _userOperationService.updateUser(userInformation);

      if(response != null){
        GlobalCache.userInformation = response;
        viewModel.getUserInfo();
        return response;
      }else{
        print(TAG+ "response null");
      }
    }else{
      print(TAG+ "user information null");
    }


  }

  final String TAG = "LoginDaysViewModel-> ";
}