import 'package:treeco/core/models/auth_request_body.dart';
import 'package:treeco/view/onboarding/signup/model/signup_model.dart';
import 'package:treeco/view/onboarding/signup/service/ISignUpService.dart';
import 'package:treeco/core/constants/enums/network_route_enum.dart';
import 'package:vexana/src/interface/INetworkService.dart';
import 'package:vexana/vexana.dart';

class SignUpService extends ISignUpService {
  SignUpService(INetworkManager manager) : super(manager);

  @override
  Future<SignUpModel> signUp(UserRequest requestBody) async {
    final response = await manager.fetch<SignUpModel, SignUpModel>(
        NetworkRoutes.SIGN_UP.rawValue,
        parseModel: SignUpModel(),
        method: RequestType.POST,
        data: requestBody);

    if(response.error != null){
      print("error: ${response.error.description}");
      return null;
    }

    return response.data;
  }

}