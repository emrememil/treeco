import 'package:treeco/core/models/auth_request_body.dart';
import 'package:treeco/view/onboarding/signup/model/signup_model.dart';
import 'package:vexana/vexana.dart';

abstract class ISignUpService {
  final INetworkManager manager;

  ISignUpService(this.manager);

  Future<SignUpModel> signUp(UserRequest userRequest);
}