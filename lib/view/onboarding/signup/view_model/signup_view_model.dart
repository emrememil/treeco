import 'package:flutter/cupertino.dart';
import 'package:mobx/mobx.dart';
import 'package:treeco/core/base/model/base_view_model.dart';
import 'package:treeco/core/constants/app/app_constants.dart';
import 'package:treeco/core/dal/network/AuthService/auth_service.dart';
import 'package:treeco/core/dal/network/UserOperations/user_operation_service.dart';
import 'package:treeco/core/models/auth_request_body.dart';
import 'package:treeco/core/models/user_information.dart';
import 'package:treeco/view/onboarding/signup/model/signup_model.dart';

part 'signup_view_model.g.dart';

class SignUpViewModel = _SignUpViewModelBase with _$SignUpViewModel;

abstract class _SignUpViewModelBase with Store, BaseViewModel {
  void setContext(BuildContext context) => this.context = context;

  void init() {}

  final AuthService service = AuthService(null);

  UserOperationService _userOperationService = UserOperationService(null);

  @observable
  SignUpModel signUpResponse = SignUpModel();

  @observable
  bool emailCheck = false;
  @observable
  bool emailFocus = false;
  @observable
  bool passwordCheck = false;
  @observable
  bool passwordFocus = false;
  @observable
  bool confirmPasswordCheck = false;
  @observable
  bool confirmPasswordFocus = false;

  @observable
  String email = "";
  @observable
  String password = "";
  @observable
  String confirmPassword = "";
  @observable
  String nameAndSurname = "";
  @observable
  String username = "";

  @action
  Future<bool> signUp() async {
    final requestBody =
        UserRequest(email: email, password: password, returnSecureToken: true);
    final response = await service.signUp(requestBody);

    if (response != null) {
      signUpResponse = response;
      final userResponse = await _userOperationService.createUser(
          UserInformationModel(
              userID: signUpResponse.localId,
              accountStatus: "free",
              coin: 0,
              dob: "",
              gender: "",
              nameAndSurname: this.nameAndSurname,
              ranking: 0,
              realTreeCount: 0,
              tel: "",
              username: this.username,
              virtualTreeCount: 0));
      if (userResponse != null) {
        return true;
      } else {
        return false;
      }
    } else {
      return false;
    }
  }

  @action
  void emailOnChanged(String val) {
    if (ApplicationConstants.emailCheck(val)) {
      emailCheck = true;
      emailFocus = true;
    } else {
      emailCheck = false;
      emailFocus = true;
    }
  }

  @action
  void passwordOnChanged(String val) {
    if (val.length >= 8) {
      passwordCheck = true;
      passwordFocus = true;
    } else {
      passwordCheck = false;
      passwordFocus = true;
    }
  }

  @action
  void confirmPasswordOnChanged(String val) {
    if (password == confirmPassword) {
      confirmPasswordCheck = true;
      confirmPasswordFocus = true;
    } else {
      confirmPasswordCheck = false;
      confirmPasswordFocus = true;
    }
  }
}
