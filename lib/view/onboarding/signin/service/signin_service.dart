import 'package:treeco/core/models/auth_request_body.dart';
import 'package:treeco/view/onboarding/signin/model/signin_model.dart';
import 'package:treeco/view/onboarding/signin/service/ISignInService.dart';
import 'package:treeco/core/constants/enums/network_route_enum.dart';
import 'package:vexana/src/interface/INetworkService.dart';
import 'package:vexana/vexana.dart';

class SignInService extends ISignInService {
  SignInService(INetworkManager manager) : super(manager);

  @override
  Future<SignInModel> fetchUserControl(UserRequest requestBody) async {
    final response = await manager.fetch<SignInModel, SignInModel>(
        NetworkRoutes.SIGN_IN.rawValue,
        parseModel: SignInModel(),
        method: RequestType.POST,
        data: requestBody);

    if(response.error != null){
      print("error: ${response.error.description}");
      return null;
    }

    return response.data;
  }

}