import 'package:treeco/core/models/auth_request_body.dart';
import 'package:treeco/view/onboarding/signin/model/signin_model.dart';
import 'package:vexana/vexana.dart';

abstract class ISignInService {
  final INetworkManager manager;

  ISignInService(this.manager);

  Future<SignInModel> fetchUserControl(UserRequest userRequest);
}