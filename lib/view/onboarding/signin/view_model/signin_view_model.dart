import 'package:flutter/cupertino.dart';
import 'package:mobx/mobx.dart';
import 'package:treeco/core/base/model/base_view_model.dart';
import 'package:treeco/core/constants/app/app_constants.dart';
import 'package:treeco/core/constants/app/GlobalCache.dart';
import 'package:treeco/core/dal/network/AuthService/auth_service.dart';
import 'package:treeco/core/dal/network/UserOperations/user_operation_service.dart';
import 'package:treeco/core/dal/network/auth_manager.dart';
import 'package:treeco/core/models/auth_request_body.dart';
import 'package:treeco/core/models/user_information.dart';
import 'package:treeco/view/onboarding/signin/model/signin_model.dart';
import 'package:treeco/view/onboarding/signin/service/ISignInService.dart';
import 'package:treeco/view/onboarding/signin/service/signin_service.dart';

part 'signin_view_model.g.dart';

class SignInViewModel = _SignInViewModelBase with _$SignInViewModel;

abstract class _SignInViewModelBase with Store,BaseViewModel{

  ISignInService signInService;

  void setContext(BuildContext context) => this.context = context;

  void init() {
    signInService = SignInService(AuthManager.instance.networkManager);
  }

  final UserOperationService _userOperationService = UserOperationService(null);

  AuthService service;
  UserInformationModel userInformation;

  @observable
  SignInModel signInResponse = SignInModel();

  @observable
  bool isLoading=false;

  @observable
  String email="";
  @observable
  String password="";

  @observable
  bool rememberMe=false;

  @observable
  bool passwordCheck=false;

  @observable
  bool emailCheck=false;

  @observable
  bool focus2=false;

  @observable
  bool focus1=false;

  @action
  Future<UserInformationModel> getUserInfo() async{
    if(GlobalCache.USER_ID != ""){
      final response = await _userOperationService.getUser(GlobalCache.USER_ID);
      if(response != null){
        userInformation = response;
        return userInformation;
      }else{
        return null;
      }
    }
  }

  @action
  Future<bool> signIn() async{
    isLoadingChange();
    final userRequest = UserRequest(email: email,password: password, returnSecureToken: true);
    final response = await signInService.fetchUserControl(userRequest);

    if(response != null){
      signInResponse = response;
      GlobalCache.USER_ID = response.localId;
      GlobalCache.userInformation = await getUserInfo();
      isLoadingChange();
      return true;
    }else{
      isLoadingChange();
      return false;
    }
  }

  @action
  void isLoadingChange(){
    isLoading = !isLoading;
  }

  @action
  void changeRememberMe(bool val){
    rememberMe=val;
  }

  @action
  void passwordOnChanged(String val){
    if(val.length>=6){
      passwordCheck=true;
      focus2=true;
    }else{
      passwordCheck=false;
      focus2=true;
    }
  }

  @action
  void emailOnChanged(String val){
    if (ApplicationConstants.emailCheck(val)) {
        emailCheck = true;
        focus1 = true;
    } else {
        emailCheck = false;
        focus1 = true;
    }
  }
}


