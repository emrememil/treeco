import 'package:flutter/cupertino.dart';
import 'package:mobx/mobx.dart';
import 'package:treeco/core/base/model/base_view_model.dart';
import 'package:treeco/core/models/user_information.dart';
import 'package:treeco/core/constants/app/GlobalCache.dart';

part 'option_code_view_model.g.dart';

class OptionCodeViewModel = _OptionCodeViewModelBase with _$OptionCodeViewModel;

abstract class _OptionCodeViewModelBase with Store,BaseViewModel{

  void setContext(BuildContext context) => this.context = context;

  void init() {}

  @observable
  UserInformationModel userInformation;

  @action
  UserInformationModel getUserInfo() {
    userInformation = GlobalCache.userInformation;
    return userInformation;
  }

}