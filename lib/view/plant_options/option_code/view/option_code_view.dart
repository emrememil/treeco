import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/material.dart';
import 'package:rflutter_alert/rflutter_alert.dart';
import 'package:treeco/core/base/view/base_widget.dart';
import 'package:treeco/core/constants/app/GlobalCache.dart';
import 'package:treeco/view/plant_options/option_code/model/option_code_model.dart';
import 'package:treeco/view/plant_options/option_code/view_model/option_code_view_model.dart';
import 'package:uuid/uuid.dart';

class OptionCodeScreen extends StatefulWidget {
  @override
  _OptionCodeScreenState createState() => _OptionCodeScreenState();
}

class _OptionCodeScreenState extends State<OptionCodeScreen> {

  OptionCodeModel optionCodeModel;
  OptionCodeViewModel optionCodeViewModel;
  TextEditingController _editTextAciklama = TextEditingController();
  bool dikildi = false;



  @override
  Widget build(BuildContext context) {
    return BaseView<OptionCodeViewModel>(
      viewModel: OptionCodeViewModel(),
      onModelReady: (model) {
        model.setContext(context);
        optionCodeViewModel = model;
      },
      onPageBuilder: (context, value) => body,
    );
  }
  get body => Scaffold(
    appBar: AppBar(
      backgroundColor: Color(0xff50A387),
      title: Text("Tek Kullanımlık Kod ile Ağaç Dik"),
    ),
    body: Container(
      width: MediaQuery.of(context).size.width,
      color: Colors.white,
      child: Padding(
        padding: const EdgeInsets.all(32.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(
              "Kodunuzu Aşağı Girin ve Ağacınızı Dikin!",
              style: TextStyle(
                  fontSize: 25,
                  fontWeight: FontWeight.bold,
                  color: Colors.black54),
              textAlign: TextAlign.center,
            ),
            SizedBox(
              height: 15,
            ),
            Container(
              decoration: BoxDecoration(
                color: Color(0xff50A387),
                borderRadius: BorderRadius.all(
                  Radius.circular(10),
                ),
              ),
              child: TextFormField(
                controller: _editTextAciklama,
                validator: (deger) {
                  if(deger.length< 1){
                    return "Bu alanı boş geçemezsiniz";
                  }else{
                    return null;
                  }
                },
                style: TextStyle(
                    color: Colors.white, fontSize: 18),
                decoration: InputDecoration(
                  contentPadding: EdgeInsets.all(15),
                  counterStyle: TextStyle(color: Colors.grey),
                  labelText: "Kod",
                  labelStyle: TextStyle(
                      color: Colors.white, fontFamily: 'Ubuntu'),
                  hintText:
                  "Tek kullanımlık kodunuzu giriniz",
                  hintStyle: TextStyle(color: Colors.white54),
                  border: InputBorder.none,
                  errorBorder: OutlineInputBorder(
                    borderSide: BorderSide(color: Colors.red),
                  ),
                ),
              ),
            ),
            SizedBox(
              height: 20,
            ),
            RaisedButton(
              onPressed: (){
                agaciDik();
              },
              color: Colors.amberAccent,
              child: Text("Kodu Onayla!", style: TextStyle(color: Colors.black54),),
            ),
          ],
        ),
      ),
    ),
  );
  void agaciDik() async{
    final databaseReference =
    FirebaseDatabase.instance.reference();
    var uuid = Uuid();
    var rastgeleAgacID = uuid.v4();
    databaseReference.once().then((DataSnapshot snapshot) {

      int reelAgacBilgisi = int.parse(snapshot.value["users"]
      [GlobalCache.USER_ID]["realTreeCount"]
          .toString());
      //print('Data : ${snapshot.value}');
      var singleKodBilgisi = snapshot.value["singleCode"]
          .toString();
      debugPrint(singleKodBilgisi.toString());

      bool varMi=singleKodBilgisi.contains(_editTextAciklama.text);
      debugPrint(varMi.toString());

      if(varMi==true && dikildi==false){

        databaseReference
            .child("dikilenAgac")
            .child(rastgeleAgacID)
            .set({
          'agacAdi':
          'agac_' + rastgeleAgacID.substring(0, 5).toString(),
          'dikilmeYolu': 'singleCode',
          'dikenID':GlobalCache.USER_ID
        });

        databaseReference.child("users").child(GlobalCache.USER_ID).update({
          'realTreeCount': reelAgacBilgisi+1
        });

        dikildi = true;
        optionCodeViewModel.getUserInfo();

        Alert(
          context: context,
          type: AlertType.success,
          title: "HARİKASIN!",
          desc: "1 ağaç daha dikerek yeşil bir dünyaya katkı sağladın",
          buttons: [
            DialogButton(
              child: Text(
                "Kapat",
                style: TextStyle(color: Colors.white, fontSize: 20),
              ),
              onPressed: () => {
                _editTextAciklama.clear(),
                Navigator.pop(context)},
              width: 120,
            )
          ],
        ).show();

      }else{
        Alert(
          context: context,
          type: AlertType.error,
          title: "SORUN VAR",
          desc: "Tek kullanımlık kod geçersiz",
          buttons: [
            DialogButton(
              child: Text(
                "Kapat",
                style: TextStyle(color: Colors.white, fontSize: 20),
              ),
              onPressed: () => Navigator.pop(context),
              width: 120,
            )
          ],
        ).show();
      }

    });
  }
}
