import 'package:barcode_scan/barcode_scan.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/material.dart';
import 'package:rflutter_alert/rflutter_alert.dart';
import 'package:treeco/core/base/view/base_widget.dart';
import 'package:treeco/core/constants/app/GlobalCache.dart';
import 'package:treeco/view/constants/size_config.dart';
import 'package:treeco/view/plant_options/option_qr/model/option_qr_model.dart';
import 'package:treeco/view/plant_options/option_qr/view_model/option_qr_view_model.dart';
import 'package:uuid/uuid.dart';

class OptionQRScreen extends StatefulWidget {
  @override
  _OptionQRScreenState createState() => _OptionQRScreenState();
}

class _OptionQRScreenState extends State<OptionQRScreen> {
  OptionQRModel optionQRModel;
  OptionQRViewModel optionQRViewModel;
  final arkaPlan = Color(0xff50A387);
  String qrResult = "Henüz Taranmadı";
  final _scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    return BaseView<OptionQRViewModel>(
      viewModel: OptionQRViewModel(),
      onModelReady: (model) {
        model.setContext(context);
        optionQRViewModel = model;
      },
      onPageBuilder: (context, value) => body,
    );
  }

  get body => Scaffold(
        appBar: AppBar(
          backgroundColor: Color(0xff50A387),
          title: Text("QR Code Ağaç Dik"),
        ),
        body: Container(
          width: MediaQuery.of(context).size.width,
          color: arkaPlan,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text(
                "SONUÇ: ",
                style: TextStyle(
                    fontSize: 25,
                    fontWeight: FontWeight.bold,
                    color: Colors.white),
                textAlign: TextAlign.center,
              ),
              SizedBox(
                height: 15,
              ),
              Text(
                qrResult,
                style: TextStyle(fontSize: 18, color: Colors.black),
                textAlign: TextAlign.center,
              ),
              SizedBox(
                height: 20,
              ),
              Container(
                width: MediaQuery.of(context).size.width - 50,
                child: FlatButton(
                  child: Text(
                    "QR Kodu Tara",
                    style: TextStyle(color: Colors.white, fontSize: 22),
                  ),
                  onPressed: () async {
                    ScanResult scanning = await BarcodeScanner.scan();
                    String texteCevir = scanning.rawContent;
                    setState(() {
                      qrResult = texteCevir;
                      var uuid = Uuid();
                      var rastgeleAgacID = uuid.v4();
                      final databaseReference =
                      FirebaseDatabase.instance.reference();

                      databaseReference.once().then((DataSnapshot snapshot) {

                        int reelAgacBilgisi = int.parse(snapshot.value["users"]
                        [GlobalCache.USER_ID]["realTreeCount"]
                            .toString());
                        //print('Data : ${snapshot.value}');
                        var qrKodBilgisi = snapshot.value["qrCode"]
                            .toString();
                        debugPrint(qrKodBilgisi.toString());

                        bool varMi=qrKodBilgisi.contains(qrResult);
                        debugPrint(varMi.toString());

                        if(varMi==true){

                          databaseReference
                              .child("dikilenAgac")
                              .child(rastgeleAgacID)
                              .set({
                            'agacAdi':
                            'agac_' + rastgeleAgacID.substring(0, 5).toString(),
                            'dikilmeYolu': 'coin',
                            'dikenID':GlobalCache.USER_ID
                          });

                          databaseReference.child("users").child(GlobalCache.USER_ID).update({
                            'realTreeCount': reelAgacBilgisi+1
                          });

                          Alert(
                            context: context,
                            type: AlertType.success,
                            title: "HARİKASIN!",
                            desc: "1 ağaç daha dikerek yeşil bir dünyaya katkı sağladın",
                            buttons: [
                              DialogButton(
                                child: Text(
                                  "Kapat",
                                  style: TextStyle(color: Colors.white, fontSize: 20),
                                ),
                                onPressed: () => Navigator.pop(context),
                                width: 120,
                              )
                            ],
                          ).show();

                        }else{
                          Alert(
                            context: context,
                            type: AlertType.error,
                            title: "SORUN VAR",
                            desc: "Taratmış olduğun QR kod geçerliliğini yitirmiş :/",
                            buttons: [
                              DialogButton(
                                child: Text(
                                  "Kapat",
                                  style: TextStyle(color: Colors.white, fontSize: 20),
                                ),
                                onPressed: () => Navigator.pop(context),
                                width: 120,
                              )
                            ],
                          ).show();
                        }


                      });
                    });
                  },
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(20.0),
                    side: BorderSide(color: Colors.white),
                  ),
                ),
              ),
            ],
          ),
        ),
      );
}
