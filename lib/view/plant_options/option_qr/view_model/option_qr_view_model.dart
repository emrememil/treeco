import 'package:flutter/cupertino.dart';
import 'package:mobx/mobx.dart';
import 'package:treeco/core/base/model/base_view_model.dart';

part 'option_qr_view_model.g.dart';

class OptionQRViewModel = _OptionQRViewModelBase with _$OptionQRViewModel;

abstract class _OptionQRViewModelBase with Store,BaseViewModel{

  void setContext(BuildContext context) => this.context = context;

  void init() {}

}