import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:lottie/lottie.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:rflutter_alert/rflutter_alert.dart';
import 'package:treeco/core/base/state/base_state.dart';
import 'package:treeco/core/base/view/base_widget.dart';
import 'package:treeco/core/constants/app/app_constants.dart';
import 'package:treeco/core/constants/app/GlobalCache.dart';
import 'package:treeco/core/constants/navigation/navigation_constants.dart';
import 'package:treeco/core/init/navigation/navigation_service.dart';
import 'package:treeco/view/constants/size_config.dart';
import 'package:treeco/view/plant_options/option_coin/model/option_coin_model.dart';
import 'package:treeco/view/plant_options/option_coin/view_model/option_coin_view_model.dart';
import 'package:treeco/core/extension/string_extension.dart';
import 'package:uuid/uuid.dart';



class OptionCoinScreen extends StatefulWidget {
  @override
  _OptionCoinScreenState createState() => _OptionCoinScreenState();
}

class _OptionCoinScreenState extends BaseState<OptionCoinScreen> {
  OptionCoinModel plantWithCoinModel;
  OptionCoinViewModel viewModel;

  final _scaffoldKey = GlobalKey<ScaffoldState>();
  int _value = 1;

  @override
  Widget build(BuildContext context) {
    return BaseView<OptionCoinViewModel>(
      viewModel: OptionCoinViewModel(),
      onModelReady: (model) {
        model.setContext(context);
        viewModel = model;
        viewModel.getUserInfo();
      },
      onPageBuilder: (context, value) => body,
    );
  }

  get body => Scaffold(
        backgroundColor: Color(ApplicationConstants.BACKGROUND_COLOR),
        key: _scaffoldKey,
        appBar: appBar,
        body: Observer(
          builder: (context) => Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              SizedBox(
                height: SizeConfig.getProportionateScreenWidth(24),
              ),
              statisticsContainer,
              SizedBox(
                height: SizeConfig.getProportionateScreenWidth(24),
              ),
              plantTreeContainer,
              Padding(
                padding: const EdgeInsets.only(left:24.0, right:24.0, top:16),
                child: Text("Ağacı Dikme Yeri", style: TextStyle(color: Colors.white, fontSize: 16),),
              ),
              Padding(
                padding: const EdgeInsets.only(left:24.0, right:24.0, top:16),
                child: Container(
                  width: MediaQuery.of(context).size.width,
                  child: DropdownButton(style: TextStyle(color: Colors.black),
                      value: _value,
                      items: [
                        DropdownMenuItem(
                          child: Text("Ankara Ağaçlandırma Projesi"),
                          value: 1,
                        ),
                        DropdownMenuItem(
                          child: Text("Sivas Ağaçlandırma Projesi"),
                          value: 2,
                        ),
                      ],
                      onChanged: (value) {
                        setState(() {
                          _value = value;
                        });
                      }),
                ),
              ),
              plantButton,
            ],
          ),
        ),
      );

  Padding get statisticsText {
    return Padding(
      padding: EdgeInsets.only(
          left: SizeConfig.getProportionateScreenWidth(8),
          top: SizeConfig.getProportionateScreenHeight(10),
          bottom: 5),
      child: Text(
        'statistics'.locale,
        style: TextStyle(
          fontSize: SizeConfig.getProportionateScreenWidth(14),
          color: Colors.white,
          fontFamily: ApplicationConstants.FONT_FAMILY,
        ),
      ),
    );
  }

  Padding get plantInfoText {
    return Padding(
      padding: EdgeInsets.only(
          left: SizeConfig.getProportionateScreenWidth(8),
          top: SizeConfig.getProportionateScreenHeight(10),
          bottom: 5),
      child: Text(
        'Dikim Bilgileri'.locale,
        style: TextStyle(
          fontSize: SizeConfig.getProportionateScreenWidth(14),
          color: Colors.white,
          fontFamily: ApplicationConstants.FONT_FAMILY,
        ),
      ),
    );
  }

  Align get plantTreeContainer {
    return Align(
      alignment: Alignment.center,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          plantInfoText,
          Container(
            width: SizeConfig.screenWidth -
                SizeConfig.getProportionateScreenWidth(32),
            height: SizeConfig.screenHeight / 4.5,
            decoration: BoxDecoration(
              color: Color(0xff11806F),
              borderRadius: BorderRadius.circular(10),
              boxShadow: [
                BoxShadow(
                  color: Colors.black.withOpacity(0.2),
                  spreadRadius: 0,
                  blurRadius: 2,
                  offset: Offset(0, 5),
                ),
              ],
            ),
            child: Row(
              children: <Widget>[
                Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    //plantTreeCount,
                    unitCost,
                    //totalCost,
                  ],
                ),
                Padding(
                  padding: EdgeInsets.only(left: 8.0, right: 8),
                  child: Lottie.asset("assets/animation/coin.json",
                      fit: BoxFit.fill,
                      width: SizeConfig.imageSizeMultiplier * 29),
                ),
              ],
            ),
          )
        ],
      ),
    );
  }

  get plantTreeCount {
    return Padding(
      padding: EdgeInsets.all(SizeConfig.getProportionateScreenWidth(8)),
      child: Container(
        width: SizeConfig.getProportionateScreenWidth(200),
        height: SizeConfig.getProportionateScreenHeight(35),
        decoration: BoxDecoration(
          color: Color(0xff58B294),
          borderRadius: BorderRadius.circular(20),
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 8.0),
              child: Text(
                'Dikilecek Ağaç Sayısı:'.locale,
                style: TextStyle(
                  fontSize: SizeConfig.getProportionateScreenWidth(14),
                  color: Colors.white,
                  fontFamily: ApplicationConstants.FONT_FAMILY,
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 8.0),
              child: Text(
                viewModel.treeCount().toString(),
                style: TextStyle(
                  fontSize: SizeConfig.getProportionateScreenWidth(14),
                  color: Colors.black,
                  fontFamily: ApplicationConstants.FONT_FAMILY,
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  get unitCost {
    return Padding(
      padding: EdgeInsets.all(SizeConfig.getProportionateScreenWidth(8)),
      child: Container(
        width: SizeConfig.getProportionateScreenWidth(200),
        height: SizeConfig.getProportionateScreenHeight(35),
        decoration: BoxDecoration(
          color: Color(0xffC8AF32),
          borderRadius: BorderRadius.circular(20),
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 8.0),
              child: Text(
                'Birim Maliyet:'.locale,
                style: TextStyle(
                  fontSize: SizeConfig.getProportionateScreenWidth(14),
                  color: Colors.white,
                  fontFamily: ApplicationConstants.FONT_FAMILY,
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 8.0),
              child: Text(
                viewModel.unitCost.toString(),
                style: TextStyle(
                  fontSize: SizeConfig.getProportionateScreenWidth(14),
                  color: Colors.black,
                  fontFamily: ApplicationConstants.FONT_FAMILY,
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  get totalCost {
    return Padding(
      padding: EdgeInsets.all(SizeConfig.getProportionateScreenWidth(8)),
      child: Container(
        width: SizeConfig.getProportionateScreenWidth(200),
        height: SizeConfig.getProportionateScreenHeight(35),
        decoration: BoxDecoration(
          color: Color(0xffFD714A),
          borderRadius: BorderRadius.circular(20),
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 8.0),
              child: Text(
                'Toplam Maliyet:'.locale,
                style: TextStyle(
                  fontSize: SizeConfig.getProportionateScreenWidth(14),
                  color: Colors.white,
                  fontFamily: ApplicationConstants.FONT_FAMILY,
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 8.0),
              child: Text(
                viewModel.userInformation == null
                    ? "-1"
                    : viewModel.userInformation.ranking.toString(),
                style: TextStyle(
                  fontSize: SizeConfig.getProportionateScreenWidth(14),
                  color: Colors.black,
                  fontFamily: ApplicationConstants.FONT_FAMILY,
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  Padding get plantButton {
    return Padding(
      padding:
          EdgeInsets.only(top: SizeConfig.getProportionateScreenHeight(64)),
      child: SizedBox(
        width: double.infinity,
        height: SizeConfig.getProportionateScreenHeight(60),
        child: Padding(
          padding: EdgeInsets.symmetric(
              horizontal: SizeConfig.getProportionateScreenWidth(24)),
          child: RaisedButton(
            shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(50)),
            color: Color(0xff11806F),
            onPressed: () {
              var uuid = Uuid();
              var rastgeleAgacID = uuid.v4();
              final databaseReference = FirebaseDatabase.instance.reference();

              print("User ID: " + GlobalCache.USER_ID);

              //Veri okuma
              databaseReference.once().then((DataSnapshot snapshot) {
                //print('Data : ${snapshot.value}');
                int coinBilgisi = int.parse(snapshot.value["users"]
                        [GlobalCache.USER_ID]["coin"]
                    .toString());

                int reelAgacBilgisi = int.parse(snapshot.value["users"]
                        [GlobalCache.USER_ID]["realTreeCount"]
                    .toString());

                if (coinBilgisi >= 100) {
                  databaseReference
                      .child("dikilenAgac")
                      .child(rastgeleAgacID)
                      .set({
                    'agacAdi':
                        'agac_' + rastgeleAgacID.substring(0, 5).toString(),
                    'dikilmeYolu': 'coin',
                    'dikenID': GlobalCache.USER_ID
                  });

                  databaseReference
                      .child("users")
                      .child(GlobalCache.USER_ID)
                      .update({'coin': coinBilgisi - 100});
                  databaseReference
                      .child("users")
                      .child(GlobalCache.USER_ID)
                      .update({'realTreeCount': reelAgacBilgisi + 1});
                  viewModel.getUserInfo();

                  Alert(
                    context: context,
                    type: AlertType.success,
                    title: "HARİKASIN!",
                    desc:
                        "1 ağaç daha dikerek yeşil bir dünyaya katkı sağladın",
                    buttons: [
                      DialogButton(
                        child: Text(
                          "Kapat",
                          style: TextStyle(color: Colors.white, fontSize: 20),
                        ),
                        onPressed: () => Navigator.pop(context),
                        width: 120,
                      )
                    ],
                  ).show();
                } else {
                  Alert(
                    context: context,
                    type: AlertType.warning,
                    title: "YETERSİZ COIN :/",
                    desc:
                        "Ağaç dikebilmek için en az 100 coine sahip olman gerekiyor",
                    buttons: [
                      DialogButton(
                        child: Text(
                          "Kapat",
                          style: TextStyle(color: Colors.white, fontSize: 20),
                        ),
                        onPressed: () => Navigator.pop(context),
                        width: 120,
                      )
                    ],
                  ).show();
                }
              });
            },
            child: Text(
              'Onayla'.locale,
              style: TextStyle(
                  fontSize: SizeConfig.getProportionateScreenWidth(13),
                  color: Colors.white,
                  fontFamily: ApplicationConstants.FONT_FAMILY),
            ),
          ),
        ),
      ),
    );
  }

  Align get statisticsContainer {
    return Align(
      alignment: Alignment.center,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          statisticsText,
          Container(
            width: SizeConfig.screenWidth -
                SizeConfig.getProportionateScreenWidth(32),
            height: SizeConfig.getProportionateScreenHeight(110),
            decoration: BoxDecoration(
              color: Color(0xff58B294),
              //borderRadius: BorderRadius.circular(10),
              boxShadow: [
                BoxShadow(
                  color: Colors.black.withOpacity(0.2),
                  spreadRadius: 0,
                  blurRadius: 2,
                  offset: Offset(0, 5),
                ),
              ],
            ),
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Column(
                children: <Widget>[
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      GestureDetector(
                        onTap: () => NavigationService.instance.navigateToPage(
                            path: NavigationConstants.LEADER_BOARD),
                        child: Container(
                          height: SizeConfig.getProportionateScreenHeight(35),
                          decoration: BoxDecoration(
                            color: Color(0xff11806F),
                            borderRadius: BorderRadius.circular(20),
                          ),
                          child: Row(
                            children: <Widget>[
                              Padding(
                                padding:
                                    const EdgeInsets.symmetric(horizontal: 8.0),
                                child: Text(
                                  'myRank'.locale,
                                  style: TextStyle(
                                    fontSize:
                                        SizeConfig.getProportionateScreenWidth(
                                            14),
                                    color: Colors.white,
                                    fontFamily:
                                        ApplicationConstants.FONT_FAMILY,
                                  ),
                                ),
                              ),
                              Padding(
                                padding:
                                    const EdgeInsets.symmetric(horizontal: 8.0),
                                child: Text(
                                  viewModel.getUserInfo() == null
                                      ? "-1"
                                      : (viewModel.userInformation.ranking+1)
                                          .toString(),
                                  style: TextStyle(
                                    fontSize:
                                        SizeConfig.getProportionateScreenWidth(
                                            14),
                                    color: Color(0xff2EE574),
                                    fontFamily:
                                        ApplicationConstants.FONT_FAMILY,
                                  ),
                                ),
                              )
                            ],
                          ),
                        ),
                      ),
                      Container(
                        height: SizeConfig.getProportionateScreenHeight(32),
                        decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(10),
                        ),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: <Widget>[
                            //Icon(MdiIcons.pineTree, color: Colors.green,)
                            Padding(
                              padding:
                                  const EdgeInsets.only(right: 4.0, left: 8),
                              child: Icon(
                                MdiIcons.pineTree,
                                color: Color(
                                    ApplicationConstants.BACKGROUND_COLOR),
                                size:
                                    SizeConfig.getProportionateScreenWidth(22),
                              ),
                            ),
                            Padding(
                              padding: EdgeInsets.only(right: 12.0),
                              child: Text(
                                viewModel.userInformation == null
                                    ? "-1"
                                    : viewModel.userInformation.realTreeCount
                                        .toString(),
                                style: TextStyle(
                                  fontSize:
                                      SizeConfig.getProportionateScreenWidth(
                                          14),
                                  color:
                                      Color(ApplicationConstants.LIGHT_GREEN),
                                  fontFamily: ApplicationConstants.FONT_FAMILY,
                                ),
                              ),
                            )
                          ],
                        ),
                      )
                    ],
                  ),
                  SizedBox(
                    height: 12,
                  ),
                  Container(
                    height: SizeConfig.getProportionateScreenHeight(35),
                    decoration: BoxDecoration(
                      color: Color(0xffF6C458),
                      borderRadius: BorderRadius.circular(20),
                    ),
                    child: Row(
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 8.0),
                          child: Text(
                            'Coin:'.locale,
                            style: TextStyle(
                              fontSize:
                                  SizeConfig.getProportionateScreenWidth(14),
                              color: Colors.black,
                              fontFamily: ApplicationConstants.FONT_FAMILY,
                            ),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 8.0),
                          child: Text(
                            viewModel.userInformation == null
                                ? "-1"
                                : viewModel.userInformation.coin.toString(),
                            style: TextStyle(
                              fontSize:
                                  SizeConfig.getProportionateScreenWidth(14),
                              color: Color(0xff50A387),
                              fontFamily: ApplicationConstants.FONT_FAMILY,
                            ),
                          ),
                        )
                      ],
                    ),
                  )
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  AppBar get appBar {
    return AppBar(
      backgroundColor: Color(ApplicationConstants.BACKGROUND_COLOR),
      elevation: 4,
      iconTheme: IconThemeData(color: Colors.white),
      title: Text(
        "Ağaç Dik",
        style: TextStyle(
            fontSize: SizeConfig.getProportionateScreenWidth(14),
            color: Colors.white,
            fontFamily: ApplicationConstants.FONT_FAMILY),
      ),
    );
  }
}
