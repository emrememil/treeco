// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'option_coin_view_model.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$OptionCoinViewModel on _OptionCoinViewModelBase, Store {
  final _$userInformationAtom =
      Atom(name: '_OptionCoinViewModelBase.userInformation');

  @override
  UserInformationModel get userInformation {
    _$userInformationAtom.reportRead();
    return super.userInformation;
  }

  @override
  set userInformation(UserInformationModel value) {
    _$userInformationAtom.reportWrite(value, super.userInformation, () {
      super.userInformation = value;
    });
  }

  final _$unitCostAtom = Atom(name: '_OptionCoinViewModelBase.unitCost');

  @override
  int get unitCost {
    _$unitCostAtom.reportRead();
    return super.unitCost;
  }

  @override
  set unitCost(int value) {
    _$unitCostAtom.reportWrite(value, super.unitCost, () {
      super.unitCost = value;
    });
  }

  final _$_OptionCoinViewModelBaseActionController =
      ActionController(name: '_OptionCoinViewModelBase');

  @override
  UserInformationModel getUserInfo() {
    final _$actionInfo = _$_OptionCoinViewModelBaseActionController.startAction(
        name: '_OptionCoinViewModelBase.getUserInfo');
    try {
      return super.getUserInfo();
    } finally {
      _$_OptionCoinViewModelBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  int treeCount() {
    final _$actionInfo = _$_OptionCoinViewModelBaseActionController.startAction(
        name: '_OptionCoinViewModelBase.treeCount');
    try {
      return super.treeCount();
    } finally {
      _$_OptionCoinViewModelBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  String toString() {
    return '''
userInformation: ${userInformation},
unitCost: ${unitCost}
    ''';
  }
}
