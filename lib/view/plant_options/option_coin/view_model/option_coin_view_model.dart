import 'package:flutter/cupertino.dart';
import 'package:mobx/mobx.dart';
import 'package:treeco/core/base/model/base_view_model.dart';
import 'package:treeco/core/models/user_information.dart';
import 'package:treeco/core/constants/app/GlobalCache.dart';
import 'package:treeco/core/dal/network/UserOperations/user_operation_service.dart';


part 'option_coin_view_model.g.dart';

class OptionCoinViewModel = _OptionCoinViewModelBase with _$OptionCoinViewModel;

abstract class _OptionCoinViewModelBase with Store,BaseViewModel{

  void setContext(BuildContext context) => this.context = context;

  void init() {}

  UserOperationService _userOperationService = UserOperationService(null);

  @observable
  UserInformationModel userInformation;

  @observable
  int unitCost = 100;

  @action
  UserInformationModel getUserInfo() {
    userInformation = GlobalCache.userInformation;
    return userInformation;
  }

  @action
  int treeCount(){
    return this.userInformation.coin~/unitCost;
  }


  Future<UserInformationModel> updateCoin(int newCoin) async {
    var userInformation = GlobalCache.userInformation;
    if (userInformation != null) {
      userInformation.coin += newCoin;
      var response = await _userOperationService.updateUser(userInformation);

      if (response != null) {
        GlobalCache.userInformation = response;
        getUserInfo();
        return response;
      } else {
        print("TAG" + "response null");
      }
    } else {
      print("TAG" + "user information null");
    }
  }
}