import 'dart:io';
import 'package:firebase_storage/firebase_storage.dart' as firebase_storage;
import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:rflutter_alert/rflutter_alert.dart';
import 'package:treeco/core/base/view/base_widget.dart';
import 'package:treeco/core/constants/app/app_constants.dart';
import 'package:treeco/core/constants/app/GlobalCache.dart';
import 'package:treeco/core/constants/navigation/navigation_constants.dart';
import 'package:treeco/core/init/navigation/navigation_service.dart';
import 'package:treeco/view/constants/size_config.dart';
import 'package:treeco/core/extension/string_extension.dart';
import 'package:treeco/view/personal/profile_edit/model/profile_edit_model.dart';
import 'package:treeco/view/personal/profile_edit/view_model/profile_edit_view_model.dart';
import 'package:hive/hive.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:uuid/uuid.dart';

class ProfileEditScreen extends StatefulWidget {
  @override
  _ProfileEditScreenState createState() => _ProfileEditScreenState();
}

class _ProfileEditScreenState extends State<ProfileEditScreen> {
  ProfileEditModel profileEditModel;
  ProfileEditViewModel profileEditViewModel;
  var formKey = GlobalKey<FormState>();
  final _scaffoldKey = GlobalKey<ScaffoldState>();

  File secilenResim;
  final picker = ImagePicker();

  @override
  Widget build(BuildContext context) {
    return BaseView<ProfileEditViewModel>(
      viewModel: ProfileEditViewModel(),
      onModelReady: (model) {
        model.setContext(context);
        profileEditViewModel = model;
      },
      onPageBuilder: (context, value) => body,
    );
  }

  get body => Scaffold(
        appBar: appBar,
        body: FutureBuilder(
          future: hivedenGetir(),
          builder: (BuildContext context, AsyncSnapshot<String> snapshots) {
            if (snapshots.hasData) {
              return Container(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    //infoText,
                    profilePicture(snapshots.data),
                    GestureDetector(onTap: () {
                      secilenBirinciResim();
                    },child: changeProfilePhotoText),
                    Form(
                      key: formKey,
                      child: Column(
                        children: <Widget>[
                          nameAndSurnameTextField,
                          usernameTextField,
                        ],
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.only(
                          top: SizeConfig.getProportionateScreenHeight(32)),
                      child: Divider(),
                    ),
                    personalInformationSettingsText,
                    Padding(
                      padding: EdgeInsets.only(
                          top: SizeConfig.getProportionateScreenHeight(8)),
                      child: Divider(),
                    ),
                    Padding(
                      padding: EdgeInsets.only(
                          top: SizeConfig.getProportionateScreenHeight(1)),
                      child: Divider(),
                    ),
                    switchToProAccountText,
                    Padding(
                      padding: EdgeInsets.only(
                          top: SizeConfig.getProportionateScreenHeight(8)),
                      child: Divider(),
                    ),
                  ],
                ),
              );
            } else {
              return CircularProgressIndicator();
            }
          },
        ),
      );

  AppBar get appBar {
    return AppBar(
      backgroundColor: Colors.white,
      iconTheme: IconThemeData(
          color: Colors.black,
          size: SizeConfig.getProportionateScreenWidth(22)),
      title: Text(
        'editProfile'.locale,
        style: TextStyle(
            fontSize: SizeConfig.getProportionateScreenWidth(14),
            color: Colors.black,
            fontFamily: ApplicationConstants.FONT_FAMILY),
      ),
      actions: <Widget>[
        GestureDetector(
          onTap: (){
            fotoyuYukle().then((value){
              Alert(
                context: context,
                type: AlertType.success,
                title: "HARİKA",
                desc: "Profil fotoğrafın başarıyla güncellendi",
                buttons: [
                  DialogButton(
                    child: Text(
                      "Kapat",
                      style: TextStyle(color: Colors.white, fontSize: 20),
                    ),
                    onPressed: () => Navigator.pop(context),
                    width: 120,
                  )
                ],
              ).show();
            });
          },
          child: Padding(
            padding:
                EdgeInsets.only(right: SizeConfig.getProportionateScreenWidth(2)),
            child: IconButton(
              icon: Icon(
                Icons.check,
                color: Colors.blue,
                size: SizeConfig.getProportionateScreenWidth(22),
              ),
            ),
          ),
        )
      ],
    );
  }

  Padding get switchToProAccountText {
    return Padding(
      padding: EdgeInsets.only(
        top: SizeConfig.getProportionateScreenHeight(8),
        left: SizeConfig.getProportionateScreenWidth(16),
      ),
      child: GestureDetector(
        onTap: () {},
        child: Text(
          'switchToProAccount'.locale,
          style: TextStyle(
            fontFamily: ApplicationConstants.FONT_FAMILY2,
            color: Colors.blue,
            fontSize: SizeConfig.getProportionateScreenWidth(13),
            fontWeight: FontWeight.bold,
          ),
        ),
      ),
    );
  }

  Padding get personalInformationSettingsText {
    return Padding(
      padding: EdgeInsets.only(
          top: SizeConfig.getProportionateScreenHeight(8),
          left: SizeConfig.getProportionateScreenWidth(16)),
      child: GestureDetector(
        onTap: () {
          NavigationService.instance
              .navigateToPage(path: NavigationConstants.INFORMATION_SETTINGS);
        },
        child: Text(
          'personalInformationSettings'.locale,
          style: TextStyle(
            fontFamily: ApplicationConstants.FONT_FAMILY2,
            color: Colors.blue,
            fontSize: SizeConfig.getProportionateScreenWidth(13),
            fontWeight: FontWeight.bold,
          ),
        ),
      ),
    );
  }

  Align get changeProfilePhotoText {
    return Align(
      alignment: Alignment.center,
      child: Text(
        'changeProfilePhoto'.locale,
        style: TextStyle(
          fontFamily: ApplicationConstants.FONT_FAMILY2,
          color: Colors.blue,
          fontSize: SizeConfig.getProportionateScreenWidth(13),
          fontWeight: FontWeight.bold,
        ),
      ),
    );
  }

  Padding get usernameTextField {
    return Padding(
      padding: EdgeInsets.symmetric(
          horizontal: SizeConfig.getProportionateScreenWidth(16)),
      child: TextFormField(
        decoration: InputDecoration(
          labelText: 'username'.locale,
          labelStyle: TextStyle(
            fontSize: SizeConfig.getProportionateScreenWidth(13),
          ),
        ),
        style: TextStyle(
            fontSize: SizeConfig.getProportionateScreenWidth(13),
            color: Colors.black,
            fontFamily: ApplicationConstants.FONT_FAMILY2),
      ),
    );
  }

  Padding get nameAndSurnameTextField {
    return Padding(
      padding: EdgeInsets.symmetric(
          horizontal: SizeConfig.getProportionateScreenWidth(16)),
      child: TextFormField(
        decoration: InputDecoration(
          labelText: 'nameAndSurname'.locale,
          labelStyle: TextStyle(
            fontSize: SizeConfig.getProportionateScreenWidth(13),
          ),
        ),
        style: TextStyle(
            fontSize: SizeConfig.getProportionateScreenWidth(13),
            color: Colors.black,
            fontFamily: ApplicationConstants.FONT_FAMILY2),
      ),
    );
  }

  profilePicture(String data) {
    return Align(
      alignment: Alignment.center,
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: CircleAvatar(
          radius: SizeConfig.getProportionateScreenWidth(78),
          backgroundColor: Color(ApplicationConstants.TEXT_GREEN),
          child: CircleAvatar(
            radius: SizeConfig.getProportionateScreenWidth(75),
            backgroundImage: NetworkImage(data),
          ),
        ),
      ),
    );
  }

  Future<String> hivedenGetir() async {
    final databaseReference =
    FirebaseDatabase.instance.reference();

    DataSnapshot _dataFirebase = await databaseReference.once();
    String profilFotoURL = _dataFirebase.value["users"]
    [GlobalCache.USER_ID]["profilePhoto"]
        .toString();
    debugPrint(profilFotoURL);

    return profilFotoURL;
  }
  void secilenBirinciResim() async {
    var resim = await ImagePicker.pickImage(source: ImageSource.gallery);
      secilenResim = resim;
      debugPrint("Resim yolu: "+secilenResim.toString());

  }

  Future<String> fotoyuYukle() async {
  debugPrint("Seçilen resim: "+ secilenResim.path.toString());
    var uuid = Uuid();
    var rastgeleAgacID = uuid.v4();
    try {
      await firebase_storage.FirebaseStorage.instance
          .ref('uploads/'+rastgeleAgacID.toString()+'.png')
          .putFile(secilenResim).then((veri) async {
        var resimLinki = await veri.ref.getDownloadURL();
        debugPrint(resimLinki.toString());

        final databaseReference =
        FirebaseDatabase.instance.reference();
        await databaseReference.child("users").child(GlobalCache.USER_ID).update({
          'profilePhoto': resimLinki
        });
        return resimLinki;
      });
    }  catch (e) {
      debugPrint("Resim yüklenemedi : "+ e.toString());
    }

  }
}
