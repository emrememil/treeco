import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:mobx/mobx.dart';
import 'package:treeco/core/base/model/base_view_model.dart';
import 'package:treeco/core/models/user_information.dart';
import 'package:treeco/core/constants/app/GlobalCache.dart';

part 'profile_view_model.g.dart';

class ProfileViewModel = _ProfileViewModelBase with _$ProfileViewModel;

abstract class _ProfileViewModelBase with Store,BaseViewModel{

  void setContext(BuildContext context) => this.context = context;

  void init() {}

  @observable
  UserInformationModel userInformation;


  @action
  UserInformationModel getUserInfo() {
    userInformation = GlobalCache.userInformation;
    return userInformation;
  }

}
