import 'package:flutter/cupertino.dart';
import 'package:mobx/mobx.dart';
import 'package:treeco/core/base/model/base_view_model.dart';
import 'package:treeco/core/models/user_information.dart';
import 'package:treeco/core/constants/app/GlobalCache.dart';

part 'home_view_model.g.dart';

class HomeViewModel = _HomeViewModelBase with _$HomeViewModel;

abstract class _HomeViewModelBase with Store,BaseViewModel{

  void setContext(BuildContext context) => this.context = context;

  void init() {}

  final String TAG = "HomeViewModel";

  @observable
  UserInformationModel userInformation;



  @action
  UserInformationModel getUserInfo() {
    userInformation = GlobalCache.userInformation;
    return userInformation;
  }


}