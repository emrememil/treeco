// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'home_view_model.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$HomeViewModel on _HomeViewModelBase, Store {
  final _$userInformationAtom =
      Atom(name: '_HomeViewModelBase.userInformation');

  @override
  UserInformationModel get userInformation {
    _$userInformationAtom.reportRead();
    return super.userInformation;
  }

  @override
  set userInformation(UserInformationModel value) {
    _$userInformationAtom.reportWrite(value, super.userInformation, () {
      super.userInformation = value;
    });
  }

  final _$_HomeViewModelBaseActionController =
      ActionController(name: '_HomeViewModelBase');

  @override
  UserInformationModel getUserInfo() {
    final _$actionInfo = _$_HomeViewModelBaseActionController.startAction(
        name: '_HomeViewModelBase.getUserInfo');
    try {
      return super.getUserInfo();
    } finally {
      _$_HomeViewModelBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  String toString() {
    return '''
userInformation: ${userInformation}
    ''';
  }
}
