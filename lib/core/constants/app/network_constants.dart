class NetworkConstants{

  static const String FIREBASE_API_KEY = "AIzaSyCMnYt0MVbJmeK0QMoRd3ktQVZr3zSqyx8";
  static const String FIREBASE_BASE_URL = "https://treeco-48973.firebaseio.com/";
  static const String FIREBASE_AUTH_BASE_URL = "https://identitytoolkit.googleapis.com/v1/accounts:";
  static const String FIREBASE_AUTH_SIGN_IN_URL = "https://identitytoolkit.googleapis.com/v1/accounts:signInWithPassword?key=$FIREBASE_API_KEY";
  static const String FIREBASE_AUTH_SIGN_UP_URL = "https://identitytoolkit.googleapis.com/v1/accounts:signUp?key=$FIREBASE_API_KEY";
}