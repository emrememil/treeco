
import 'package:treeco/core/constants/app/network_constants.dart';

enum NetworkRoutes{
  SIGN_IN,
  SIGN_UP
}

extension NetworkRoutesValue on NetworkRoutes{
  String get rawValue{
    switch(this){
      case NetworkRoutes.SIGN_IN:
        return "signInWithPassword?key=${NetworkConstants.FIREBASE_API_KEY}";
      case NetworkRoutes.SIGN_UP:
        return "signUp?key=${NetworkConstants.FIREBASE_API_KEY}";
        break;
      default:
        throw "Network path not found";
    }
  }
}