import 'package:treeco/core/constants/app/network_constants.dart';
import 'package:vexana/vexana.dart';

class AuthManager {
  static AuthManager _instance;
  static AuthManager get instance {
    if(_instance == null) _instance = AuthManager._init();
    return _instance;
  }

  AuthManager._init();

  INetworkManager networkManager = NetworkManager(isEnableLogger: true, options: BaseOptions(baseUrl: NetworkConstants.FIREBASE_AUTH_BASE_URL));
}
