import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:treeco/core/constants/app/network_constants.dart';
import 'package:treeco/core/models/user_information.dart';
import 'package:vexana/vexana.dart';

class UserOperationService{
  final BuildContext context;

  UserOperationService(this.context);

  INetworkManager networkManager = NetworkManager(
      options: BaseOptions(baseUrl: NetworkConstants.FIREBASE_BASE_URL),
      isEnableLogger: true);

  Future<UserInformationModel> createUser(UserInformationModel information) async {
    final response = await networkManager.fetch<UserInformationModel, UserInformationModel>(
        "users/"+information.userID+".json",
        parseModel: UserInformationModel(),
        method: RequestType.PUT,
        data: information);

    if(response.error != null){
      print("error: ${response.error.description}");
      showAboutDialog(context: context);
    }

    return response.data;
  }

  Future<UserInformationModel> getUser(String userID) async {
    final response = await networkManager.fetch<UserInformationModel, UserInformationModel>(
        "users/"+userID+".json",
        parseModel: UserInformationModel(),
        method: RequestType.GET,
        );

    if(response.error != null){
      print("error: ${response.error.description}");
      showAboutDialog(context: context);
    }

    return response.data;
  }

  Future<UserInformationModel> updateUser(UserInformationModel information) async {
    final response = await networkManager.fetch<UserInformationModel, UserInformationModel>(
        "users/"+information.userID+".json",
        parseModel: UserInformationModel(),
        method: RequestType.PUT,
        data: information);

    if(response.error != null){
      print("error: ${response.error.description}");
      showAboutDialog(context: context);
    }

    return response.data;
  }
}