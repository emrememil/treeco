import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:treeco/core/constants/app/network_constants.dart';
import 'package:treeco/core/dal/network/AuthService/auth_path.dart';
import 'package:treeco/core/models/auth_request_body.dart';
import 'package:treeco/view/onboarding/signin/model/signin_model.dart';
import 'package:treeco/view/onboarding/signup/model/signup_model.dart';
import 'package:vexana/vexana.dart';

class AuthService {

  final BuildContext context;

  AuthService(this.context);

  INetworkManager networkManager = NetworkManager(
      options: BaseOptions(baseUrl: NetworkConstants.FIREBASE_AUTH_BASE_URL),
      isEnableLogger: true);

  Future<SignInModel> signIn(UserRequest requestBody) async {
    final response = await networkManager.fetch<SignInModel, SignInModel>(
        AuthPath.SIGN_IN.rawValue,
        parseModel: SignInModel(),
        method: RequestType.POST,
        data: requestBody);

    if(response.error != null){
      print("error: ${response.error.description}");
      showAboutDialog(context: context);
    }

    return response.data;
  }

  Future<SignUpModel> signUp(UserRequest requestBody) async {
    final response = await networkManager.fetch<SignUpModel, SignUpModel>(
        AuthPath.SIGN_UP.rawValue,
        parseModel: SignUpModel(),
        method: RequestType.POST,
        data: requestBody,);

    if(response.error != null){
      print("error: ${response.error.description}");
      showAboutDialog(context: context);
    }

    return response.data;
  }


}
