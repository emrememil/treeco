import 'package:treeco/core/constants/app/network_constants.dart';

enum AuthPath{
  SIGN_IN,
  SIGN_UP
}

extension StringPathValue on AuthPath{
  String get rawValue{
    switch(this){
      case AuthPath.SIGN_IN:
        return "signInWithPassword?key=${NetworkConstants.FIREBASE_API_KEY}";
      case AuthPath.SIGN_UP:
        return "signUp?key=${NetworkConstants.FIREBASE_API_KEY}";
        break;
      default:
        throw "Network path not found";
    }
  }
}