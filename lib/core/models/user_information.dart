import 'package:vexana/vexana.dart';

class UserInformationModel extends INetworkModel<UserInformationModel>{
  String userID;
  String accountStatus;
  int coin;
  String dob;
  String gender;
  String nameAndSurname;
  int ranking;
  int realTreeCount;
  String tel;
  String username;
  int virtualTreeCount;
  String profilePhoto;

  UserInformationModel(
      {this.userID,
        this.accountStatus,
        this.coin,
        this.dob,
        this.gender,
        this.nameAndSurname,
        this.ranking,
        this.realTreeCount,
        this.tel,
        this.username,
        this.virtualTreeCount,
      this.profilePhoto});

  UserInformationModel.fromJson(Map<String, dynamic> json) {
    userID = json['userID'];
    accountStatus = json['accountStatus'];
    coin = json['coin'];
    dob = json['dob'];
    gender = json['gender'];
    nameAndSurname = json['nameAndSurname'];
    ranking = json['ranking'];
    realTreeCount = json['realTreeCount'];
    tel = json['tel'];
    username = json['username'];
    virtualTreeCount = json['virtualTreeCount'];
    profilePhoto = json['profilePhoto'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['userID'] = this.userID;
    data['accountStatus'] = this.accountStatus;
    data['coin'] = this.coin;
    data['dob'] = this.dob;
    data['gender'] = this.gender;
    data['nameAndSurname'] = this.nameAndSurname;
    data['ranking'] = this.ranking;
    data['realTreeCount'] = this.realTreeCount;
    data['tel'] = this.tel;
    data['username'] = this.username;
    data['virtualTreeCount'] = this.virtualTreeCount;
    data['profilePhoto'] = this.profilePhoto;
    return data;
  }

  @override
  UserInformationModel fromJson(Map<String, Object> json) => UserInformationModel.fromJson(json);
}

